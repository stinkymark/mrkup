const gulp = require('gulp');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const inject = require('gulp-inject');

const devFolder = './dev';
const buildFolder = './res';

gulp.task('views', function buildHTML() {
  return gulp.src(`${devFolder}/views/index.pug`)
  .pipe(pug())
  .pipe(gulp.dest(`${buildFolder}`))
});

gulp.task('inject', function injectingLibs() {
	const target = gulp.src(`${buildFolder}/index.html`);
	const sources = gulp.src([
		'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
		'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css',
		'https://code.jquery.com/jquery-3.1.0.min.js',
		'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'
	], {read: false});

	return target.pipe(inject(sources))
    .pipe(gulp.dest(`${buildFolder}`));
})

gulp.task('styles', function buildStyles() {
  return gulp.src(`${devFolder}/styles/main.scss`)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(`${buildFolder}/css`));
});

gulp.task('watch', function () {
  gulp.watch([`${devFolder}/styles/*.scss`, `${devFolder}/views/**/*.pug`, ], ['styles', 'views']);
});
